﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    [SerializeField]
    private AudioClip shoot;
    [SerializeField]
    private AudioClip lazerShoot;
    [SerializeField]
    AudioSource audiosource;
    // Use this for initialization
    private static SoundManager instance;
    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<SoundManager>();
            return instance;
        }
    }
	public void  PlayAudio(int number)
    {
        if (number == 0)
            audiosource.PlayOneShot(shoot);
        else
            audiosource.PlayOneShot(lazerShoot);

    }

}
