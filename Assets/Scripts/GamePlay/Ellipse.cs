﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Ellipse
{
    public float xAxis;
    public float yAxis;

    public Ellipse(float xAxis , float yAxis )
    {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }
    public Vector2 Evaluate(float t, float xOrigin, float yOrigin)
    {
        float angle = 360 * Mathf.Deg2Rad * t;
        float x = Mathf.Cos(angle) * xAxis + xOrigin;
        float y = Mathf.Sin(angle) * yAxis + yOrigin;
        return new Vector2(x, y);
    }
}
