﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitMotion : MonoBehaviour
{
    public Ellipse orbitPath;
    public float orbitPeriod;
    [Range(0,1)]
    public float orbitProgress;
    bool orbitActive = true;
    float originX;
    float originY;
    public GameObject Origin;
    bool set = false;
    private void Start()
    {
        originX = Origin.transform.position.x;
        originY = Origin.transform.position.y;

    }
    private void Update()
    {
        if (!GameState.isStart)
            return;
        if (!set)
        {
            StartCoroutine(Orbit());
            set = true;
        }
    }
    IEnumerator Orbit()
    {
        while(orbitActive)
        {
            originX = Origin.transform.position.x;
            originY = Origin.transform.position.y;
            orbitProgress += Time.deltaTime * (1 / orbitPeriod);
            orbitProgress %= 1;
            transform.position = orbitPath.Evaluate(orbitProgress , originX , originY);
            yield return null;
        }
    }

}
