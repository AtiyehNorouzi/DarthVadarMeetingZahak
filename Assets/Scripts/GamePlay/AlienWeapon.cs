﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienWeapon : MonoBehaviour
{
    [HideInInspector]
    public Player PlayerToFollow;
    [SerializeField]
    private Transform bulletPosition;
    [SerializeField]
    private AlienBullet alienBullet;
    [SerializeField]
    private float raduis = 2;
    [SerializeField]
    private LayerMask layer;
    GameObject hit;
    float bulletTime = 2;
    float timer = 0;
    Collider[] colliders;

    private void Update()
    {
        colliders = Physics.OverlapSphere(transform.position, raduis , layer);
        if(colliders.Length > 0)
        {
            if (PlayerToFollow != null && colliders[0].gameObject == PlayerToFollow.gameObject)
                return;
            if (Time.time >= bulletTime + timer)
            {
                hit = colliders[0].gameObject;
                CreateBullet();
                timer = Time.time;
            }
        }
    }
    void CreateBullet()
    {
        float angle = Mathf.Atan2(transform.position.y - hit.transform.position.y,
        transform.position.x - hit.transform.position.x) * Mathf.Rad2Deg;
        Quaternion rotateTo = Quaternion.AngleAxis(angle, Vector3.forward);
        Debug.Log("zavieee      " + angle);
        transform.rotation = rotateTo;
        AlienBullet bullet = Instantiate(alienBullet, bulletPosition.position, bulletPosition.rotation) as AlienBullet;
        bullet.Init(-bullet.transform.right, 2);
    }
}
