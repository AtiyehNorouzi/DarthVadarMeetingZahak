﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Alien : Attractable 
{
	[SerializeField]
	private Planet planet;
	[SerializeField]
	private Rigidbody rb;
	public float dir;
	float speed=1;
    public Player playerToFollow;
    public RaycastHit hit;
    [SerializeField]
    private Text Owner_txt;
    [SerializeField]
    private AlienWeapon weapon;

    void Update ()
	{
        if (!GameState.isStart)
            return;

        planet.Attract(this, rb, 2);
        transform.position += dir * transform.TransformDirection(Vector3.right) * speed * Time.deltaTime;
	}

    public void OnCollideBullet(Player player)
    {
        playerToFollow = player;
        weapon.PlayerToFollow = player;
        if (playerToFollow.type == 0)
            Owner_txt.text = "Zahak";
        else
            Owner_txt.text = "Darth";
    }
    public bool IsSamePlayer(Player player)
    {
        if (player == playerToFollow)
            return true;
        return false;
    }

}





