﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckJoin : MonoBehaviour
{
    [SerializeField]
    private GameObject panel;
    bool check = false;
    public void OnCreateGame()
    {
        check = true;
        panel.SetActive(true);
        gameObject.SetActive(true);
    }

	void Update ()
    {
		if(GameReference.Player1 !=null && GameReference.Player2 !=null && check)
        {
            MenuCode.instance.DisableMenus();
            check = false;
            GameState.isStart = true;
            panel.SetActive(false);
            gameObject.SetActive(false);
        }
	}
}
