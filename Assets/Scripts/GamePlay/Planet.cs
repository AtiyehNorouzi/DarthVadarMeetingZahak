﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : Attractor
{
    private EllipseRenderer eRenderer;
    public int orbitTimes;
    public bool taken = false;
    private void Start()
    {
        eRenderer = GetComponent<EllipseRenderer>();
        if(!eRenderer.NoTakeOver)
            eRenderer.SetOrbitTimes(orbitTimes);
    }
    void Update () 
    {
        if (GameReference.Player2 != null)
        {
            Attract (GameReference.Player2, GameReference.Player2.rb , 1);
        }
        if (GameReference.Player1 != null)
        {
            Attract (GameReference.Player1, GameReference.Player1.rb , 0);
        }

    }
    public void TakeOverPlanet(Player player)
    {
        if (!eRenderer.NoTakeOver)
        {
            if (canTakeOver[(int)player.type] && !taken)
            {
                eRenderer.CalculateTakeOverPlanet(player , player.downPosition.position, orbitTimes, out taken);
                if (taken)
                {
                    player.CmdTakeOverPlanet(eRenderer.orbitTime.gameObject);
                    eRenderer.ClearPoints();
                }
            }
            else
            {
                StopTakeOver(player);
            }
        }
    }
    public void StopTakeOver(Player player)
    {
        if (!eRenderer.NoTakeOver)
            eRenderer.SetOrbitSprites(player , orbitTimes);
        eRenderer.ClearPoints();
    }
}