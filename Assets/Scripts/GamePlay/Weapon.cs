﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class Weapon : NetworkBehaviour {
    public Bullet bulletPrefab;
    private NHNetworkedPool bulletPool;
    [SerializeField]
    private GameObject weapon;
    [SerializeField]
    private GameObject target;
    private Sprite[] weaponSprites;

    public GameReference.WeaponStruct[] weapons;

    [SerializeField]
    private Transform bulletStartPosition;

    [SerializeField]
    SpriteRenderer spriteRenderer;

    [SerializeField]
    private GameObject enemyParent;

    [SerializeField]
    private GameObject circle;
    [SyncVar]
    int currentWeapon = 0;
    bool canSlide = true;
    const float coolDown = 0.025f;
    const float warmUp = 0.05f;
    int playerType = 0;

    void Start () {
        if (NetworkServer.active)
            bulletPool = GameObject.FindObjectOfType<NHNetworkedPool> ();
    }
    public void InitWeapon (int type) {
        playerType = type;
        weaponSprites = type == 0 ? GameReference.Instance.player1WeaponSprites : GameReference.Instance.player2WeaponSprites;
        weapons = type == 0 ? GameReference.Instance.player1Weapons : GameReference.Instance.player2Weapons;
        if (isLocalPlayer) {
            GameReference.Instance.player1UIPanel.SetActive (type == 0 ? true : false);
            GameReference.Instance.player2UIPanel.SetActive (type == 0 ? false : true);
            for (int i = 0; i < weapons.Length; i++) {
                weapons[i].currentCapacity = weapons[i].bulletCapacity;
                StartCoroutine (WeaponTimeCounter (i));
                if (weapons[i].reload)
                    StartCoroutine (CoolDownWeapon (i));
            }
            StartCoroutine (RotateToTarget ());
        } else
            target.SetActive (false);
    }
    public void InitBullet () {
        if (weapons[currentWeapon].currentCapacity > 0 && weapons[currentWeapon].canShoot && !weapons[currentWeapon].OnReload) {
            CmdSpawn (currentWeapon);
            weapons[currentWeapon].currentCapacity_txt.text = (--weapons[currentWeapon].currentCapacity).ToString ();
            ResetWeaponTimer ();
            WarmUpWeapon ();
        }
    }

    [Command]
    void CmdSpawn (int cWeapon) {
        GameObject bulletGO;
        if (playerType == 0)
            SoundManager.Instance.PlayAudio(0);
        else
            SoundManager.Instance.PlayAudio(1);
        bulletPool.InstantiateFromPool (bulletStartPosition.position, weapon.transform.rotation, out bulletGO);
        if (playerType == 1 && cWeapon == 1)
            bulletGO.GetComponent<Bullet>().RpcLazerShoot(bulletGO.transform.position, -bulletGO.transform.right);
        else
        {
            if (cWeapon != 3)
                bulletGO.GetComponent<Bullet>().Init(dir: -bulletGO.transform.right, speed: weapons[cWeapon].bulletSpeed, t: (Bullet.bulletType)cWeapon, playerType: playerType );
            else
            {
                bulletGO.GetComponent<Bullet>().Init(dir: -bulletGO.transform.right, speed: weapons[cWeapon].bulletSpeed, t: (Bullet.bulletType)cWeapon, player: playerType == 0 ? GameReference.Player2.gameObject : GameReference.Player1.gameObject, playerType: playerType);
            }
        }
    }

    public void ChangeWeapon () {
        weapons[currentWeapon].weaponUI.SetActive (false);
        CmdChangeWeapon();
    }
    void WarmUpWeapon () {
        if (weapons[currentWeapon].reload) {
            weapons[currentWeapon].reload_slider.fillAmount += warmUp;
            if (Utility.AlmostEqual (weapons[currentWeapon].reload_slider.fillAmount, 1, 0.05f))
                weapons[currentWeapon].OnReload = true;
        }
    }
    [Command]
    void CmdChangeWeapon()
    {
        RpcChangeSprite();
    }
    [ClientRpc]
    void RpcChangeSprite()
    {
        if (currentWeapon == weapons.Length - 1)
            currentWeapon = 0;
        else
            currentWeapon++;
        weapons[currentWeapon].weaponUI.SetActive(true);
        spriteRenderer.sprite = weaponSprites[currentWeapon];
    }
    IEnumerator RotateToTarget () {
        while (true) {
                if (!isLocalPlayer) {
                    target.SetActive (false);
                    yield break;
                }                        
                float angle = Mathf.Atan2 (weapon.transform.localPosition.y - target.transform.localPosition.y,
                weapon.transform.localPosition.x - target.transform.localPosition.x) * Mathf.Rad2Deg;  
                Quaternion rotateTo = Quaternion.AngleAxis (angle  , Vector3.forward);
                weapon.transform.localRotation = Quaternion.Slerp (weapon.transform.localRotation, rotateTo, Time.deltaTime * 10f);
            yield return null;
        }
    }
    public float GetWeaponDamage () {
        return weapons[currentWeapon].damageRate;
    }
    IEnumerator CoolDownWeapon (int index) {
        float timer = 0;
        while (true) {
            if (weapons[index].OnReload) {
                timer += Time.deltaTime * coolDown;
                weapons[index].reload_slider.fillAmount = Mathf.Lerp (1, 0, timer);
                if (Utility.AlmostEqual (weapons[index].reload_slider.fillAmount, 0, 0.05f)) {
                    weapons[index].reload_slider.fillAmount = 0;
                    weapons[index].OnReload = false;
                    timer = 0;
                }
            }
            yield return null;
        }
    }
    IEnumerator WeaponTimeCounter (int index) {
        while (canSlide) {
            if (!weapons[index].canShoot) {
                weapons[index].timer += Time.deltaTime * 1f / weapons[index].timeToShoot;
                weapons[index].timeToShoot_slider.fillAmount = Mathf.Lerp (0, 1, weapons[index].timer);
                if (Utility.AlmostEqual (weapons[index].timer, 1, 0.05f))
                    weapons[index].canShoot = true;
            }
            yield return null;
        }
    }
    void ResetWeaponTimer () {
        weapons[currentWeapon].canShoot = false;
        weapons[currentWeapon].timer = 0;
        weapons[currentWeapon].timeToShoot_slider.fillAmount = 0;
    }
    public void AddCapacity (int type, int quantity) {
        if (weapons[type].currentCapacity + quantity <= weapons[type].bulletCapacity)
            weapons[type].currentCapacity += quantity;
        else
            weapons[type].currentCapacity = weapons[type].bulletCapacity;
        weapons[type].currentCapacity_txt.text = weapons[type].currentCapacity.ToString ();
    }
    public void StartThrowBomb (Vector3 throwPoint) {
        StartCoroutine (ThrowBomb (throwPoint));
    }
    IEnumerator ThrowBomb (Vector3 throwPoint) {
        circle.transform.position = throwPoint;
        circle.SetActive (true);
        for (int i = 0; i < enemyParent.transform.childCount; i++) {
            if (PointInsideCircle (enemyParent.transform.GetChild (i).gameObject.transform.position, throwPoint, weapons[2].inffectRaduis)) {
              //  enemyParent.transform.GetChild (i).GetComponent<Enemy> ().Damage ();
            }
        }
        yield return new WaitForSeconds (1.5f);
        circle.SetActive (false);
    }
    bool PointInsideCircle (Vector3 point, Vector3 center, float radius) {
        return Vector3.Distance (point, center) < radius;
    }
}