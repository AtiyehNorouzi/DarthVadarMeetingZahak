﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EllipseRenderer : MonoBehaviour
{
    [SerializeField]
    public SpriteRenderer orbitTime;
    Color[] lineColors;
    struct myLine
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };
    LineRenderer lr;
    [Range(3, 40)]
    public int segments;
    public Ellipse ellipse;
    public float xOrigin;
    public float yOrigin;
    public bool IsOrigin;
    public bool NoTakeOver;
    List<Vector3> points;
    int currentOrbitSprite;
    int currentColor = 0;
    public bool ValidateAtStart = false;

    void Start ()
    {
      
        lineColors = GameReference.Instance.lineColors;
        if(IsOrigin)
        {
            xOrigin = transform.position.x;
            yOrigin = transform.position.y;
        }
        lr = GetComponent<LineRenderer>();
        lr.startColor = lr.endColor = lineColors[currentColor];
        points = new List<Vector3>();
        if(ValidateAtStart)
            OnValidate();
    }
    void CalculateEcllipse()
    {
        lr.positionCount = segments + 1;
        for (int i =0;i < segments;i++)
        {
            points.Add(ellipse.Evaluate((float)i / (float)segments , xOrigin , yOrigin));
            lr.SetPosition(i , points[i]);
        }
        points.Add(points[0]);
        lr.SetPosition(segments , points[0]);
    }
    public void CalculateTakeOverPlanet(Player player, Vector3 pointNow , int orbitTimes , out bool taken )
    {
        if (isLineCollide())
        {
            points.Clear();
            currentColor++;
            currentOrbitSprite--;
            if (currentColor == orbitTimes)
            {
                taken = true;
                player.CmdTakeOverPlanet(orbitTime.gameObject);
                return;
            }
            player.SetPlanetSprite(orbitTime.gameObject, currentOrbitSprite);
            lr.startColor = lr.endColor = lineColors[currentColor];

        }
        if (points.Count == 0)
        {
            points.Add(pointNow);
        }
        else
        {
            if(GetDistance(pointNow , 0.2f))
            {
                points.Add(pointNow); 
            }
        }
        taken = false;
        lr.positionCount = points.Count;
        lr.SetPosition(points.Count - 1, points[points.Count - 1]);
    }
    bool GetDistance(Vector3 pointNow , float dist)
    {
        if (Vector3.Distance(pointNow, points[points.Count - 1]) >= dist)
            return true;
        return false; 
    }
    private void OnValidate()
    {      
        if(lr != null)
            CalculateEcllipse();
    }
    public void ClearPoints()
    {
        points.Clear();
        lr.positionCount = points.Count;
        currentColor = 0;
        lr.startColor = lr.endColor = lineColors[currentColor];

    }
    public void SetOrbitSprites(Player player , int orbitTimes)
    {
        currentOrbitSprite = orbitTimes - 1;
        player.SetPlanetSprite(orbitTime.gameObject, currentOrbitSprite);
    }
    public void SetOrbitTimes(int count)
    {
        currentOrbitSprite = count - 1;
        orbitTime.sprite = GameReference.Instance.orbitSprites[currentOrbitSprite];
    }
    private bool isLineCollide()
    {
        if (points.Count < 2)
            return false;
        int TotalLines = points.Count - 1;
        myLine[] lines = new myLine[TotalLines];
        if (TotalLines > 1)
        {
            for (int i = 0; i < TotalLines; i++)
            {
                lines[i].StartPoint = points[i];
                lines[i].EndPoint = points[i + 1];
            }
        }
        for (int i = 0; i < TotalLines - 1; i++)
        {
            myLine currentLine;
            currentLine.StartPoint = points[points.Count - 2];
            currentLine.EndPoint = points[points.Count - 1];
            if (isLinesIntersect(lines[i], currentLine))
                return true;
        }
        return false;
    }
    //    -----------------------------------    
    //    Following method checks whether given two points are same or not
    //    -----------------------------------    
    private bool checkPoints(Vector3 pointA, Vector3 pointB)
    {
        return (pointA.x == pointB.x && pointA.y == pointB.y);
    }
    //    -----------------------------------    
    //    Following method checks whether given two line intersect or not
    //    -----------------------------------    
    private bool isLinesIntersect(myLine L1, myLine L2)
    {
        if (checkPoints(L1.StartPoint, L2.StartPoint) ||
            checkPoints(L1.StartPoint, L2.EndPoint) ||
            checkPoints(L1.EndPoint, L2.StartPoint) ||
            checkPoints(L1.EndPoint, L2.EndPoint))
            return false;

        return ((Mathf.Max(L1.StartPoint.x, L1.EndPoint.x) >= Mathf.Min(L2.StartPoint.x, L2.EndPoint.x)) &&
            (Mathf.Max(L2.StartPoint.x, L2.EndPoint.x) >= Mathf.Min(L1.StartPoint.x, L1.EndPoint.x)) &&
            (Mathf.Max(L1.StartPoint.y, L1.EndPoint.y) >= Mathf.Min(L2.StartPoint.y, L2.EndPoint.y)) &&
            (Mathf.Max(L2.StartPoint.y, L2.EndPoint.y) >= Mathf.Min(L1.StartPoint.y, L1.EndPoint.y))
               );
    }
}
