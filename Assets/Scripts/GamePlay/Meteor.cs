﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Meteor : NetworkBehaviour
{
    public enum MeteorType { Pink =0 , Gray };
    [SerializeField]
    private ParticleSystem[] rockHit_particles;
    public MeteorType type;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "planet")
            CmdRockHit();
        else if(collision.gameObject.tag == "border")
        {
            if(collision.gameObject.GetComponent<Borderforce>().type == Borderforce.EdgeType.Up)
                CmdRockHit();
        }
    }
    [Command]
    void CmdRockHit()
    {
        ParticleSystem rHit = Instantiate(rockHit_particles[(int)type], transform.position, transform.rotation) as ParticleSystem;
        NetworkServer.Spawn(rHit.gameObject);
        NetworkServer.Destroy(gameObject);
    }
   
}
