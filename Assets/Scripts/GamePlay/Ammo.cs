﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
public class Ammo : NetworkBehaviour
{
    public enum ammoType { weapon1 =0 , weapon2 , weapon3 , weapon4 , weapon5 , hp , alien};
    [SerializeField]
    private GameObject ammoPrize;
    [SerializeField]
    private Sprite[] ammoSPrites;
    [SerializeField]
    private ParticleSystem getprize_particle;
    private Collider boxCollider;
    bool set = false;
    private void Awake()
    {
        boxCollider = GetComponent<Collider>();
    }
    private void Update()
    {
        if((isServer || isClient) && !set)
        {
            set = true;
            gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            Debug.Log("islocalAuthority" + isLocalPlayer);
            EnableCollider(false);
            CmdSpawnAmmo(other.gameObject);
        }
    }
    public void EnableCollider(bool enable)
    {
        boxCollider.enabled = enable;
    }
    [Command]
    void CmdSpawnAmmo(GameObject go)
    {
        GameObject ammoprize = Instantiate(ammoPrize, transform.position, transform.rotation);
        ammoprize.transform.SetParent(gameObject.transform);
        ammoPrize.transform.localPosition = Vector3.zero;
        ammoprize.transform.localScale = new Vector3(1, 1, 1);
        NetworkServer.Spawn(ammoprize);
        ParticleSystem prize_particle = Instantiate(getprize_particle, transform.position, transform.rotation) as ParticleSystem;
        NetworkServer.Spawn(prize_particle.gameObject);
        int random, amount = 0;
        GenerateRandomAmmo(go.GetComponent<Player>(), ammoprize , out random , out amount);
        RpcSetAmmo(go, ammoprize , random , amount);
        SceneControl.Instance.RemoveAmmo();
    }
    [ClientRpc]
    void RpcSetAmmo(GameObject player, GameObject prize , int random , int amount)
    {
        prize.transform.GetChild(0).GetComponent<Image>().sprite = ammoSPrites[random];
        prize.transform.GetChild(1).GetComponent<Text>().text = amount.ToString();
        prize.transform.SetParent(null);
        gameObject.SetActive(false);
    }
    void GenerateRandomAmmo(Player player  , GameObject prize , out int random , out int amount)
    {
        random = UnityEngine.Random.Range(0, Enum.GetValues(typeof(ammoType)).Length);

        if (random < 5)//weapon
        {
            amount = Mathf.FloorToInt(Mathf.Clamp(player.weapon.weapons[random].bulletCapacity * 0.1f, 1, 30));
            player.weapon.AddCapacity(random, amount);
        }
        else
        {
            amount = UnityEngine.Random.Range(1, 3);
            player.AddHP(amount * 0.1f);
        }
    }
}
