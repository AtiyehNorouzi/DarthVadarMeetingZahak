﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player : Attractable
{
    public enum playerType {  player1 =0 , player2 };

    //UI
    [SerializeField]
    private Image[] hp_slider;

    [SerializeField]
    private GameObject playerHP;

    [SerializeField]
    private Image hp_fillamount;

    [SerializeField]
    private Text playerName_txt;

    [SerializeField]
    private Sprite [] playerSprites;

    [SerializeField]
    public Weapon weapon;

    [SerializeField]
    private ParticleSystem damage_particle;

    [SerializeField]
    private CameraMovement cameraMove;

    [SerializeField]
    public SpriteRenderer spriteRenderer;

    public Transform downPosition;

    [SerializeField]
    private ParticleSystem takeOver_particle;

    [SerializeField]
    private GameObject flag;

    [SerializeField]
    private Sprite[] playerFlags;

    [SerializeField]
    private Sprite[] playerNames;

    [SerializeField]
    private Transform[] alienpPlaces;

    [SerializeField]
    private Sprite[] orbitSprites;

    public Transform[] AlienPlaces
    {
        get
        {
            return alienpPlaces;
        }
    }

    //variables 

    [SyncVar]
    public int serverType = 0;

    [SyncVar]
    public string playerName;

    public playerType type;

    public Rigidbody rb;

    Vector2 movement = Vector2.zero;

    Vector2 prevMovement = Vector3.zero;

	Vector2 transformDir;

    float dir = -1;

    float speed = 2f;

    bool previousDir = false;

    Vector3 rotation = new Vector3(0,180,0);

    const float movementSpeed = 2f;

    bool alienCollide = false;

    bool firstAssign = false;
    public override void OnStartLocalPlayer()
    {
        playerHP.SetActive(false);
        isGrounded = false;
    }
    [Server]
    private void OnServerCall()
    {
        serverType = LanNetworkManager.instance.GetServerType();
        RpcSetServerType(serverType);
        SetPlayer();
    }
    void SetPlayer()
    {
        if(isLocalPlayer)
            type = (playerType)(isServer ? serverType : (1 - serverType));
        else
        {
            if (!isServer)
                type = (playerType)(serverType);
            else if (isClient)
                type = (playerType)(1 - serverType);
        }
        InitPlayer((int)type);
    }
    void ResetComponents()
    {
       GameReference.Instance.player_hps[0].fillAmount = GameReference.Instance.player_hps[1].fillAmount= 1;
    }
    [ClientRpc]
    void RpcSetServerType(int type)
    {
        serverType = type;
        SetPlayer();
    }
    private void Awake()
    {
        orbitSprites = GameReference.Instance.orbitSprites;
    }
    void Start()
    {
        ResetComponents();
        if(isServer)
            OnServerCall();
        if (!isLocalPlayer && !isServer)
            SetPlayer();
        hp_slider = GameReference.Instance.player_hps;
        if(hasAuthority)
            CmdGiveName(LanNetworkManager.instance.playerName);
    }

    void InitPlayer(int t)
    {
        spriteRenderer.sprite = playerSprites[t];
        SetPlayerReference((playerType)t);
        weapon.InitWeapon(t);
    }
    void SetPlayerReference(playerType t)
    {
        if (t == playerType.player1)
            GameReference.Player1 = this;
        else
            GameReference.Player2 = this;
    }
    void Update()
    {
       if (!GameState.isStart)
            return;
       else
        {
            if (!firstAssign && isLocalPlayer)
            {
                cameraMove = GameObject.FindObjectOfType<CameraMovement>();
                cameraMove.AssignPlayer(gameObject);
                firstAssign = true;
            }
        }
        playerName_txt.text = playerName;
        if (!isLocalPlayer)
            return;
        if (isGrounded && Input.GetButton("Jump"))
        {
            transform.parent = null;
            rb.AddForce(transform.up *7f , ForceMode.Impulse);
            isGrounded = false;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            weapon.ChangeWeapon();
        }
        if (Input.GetMouseButton(0))
        {
          Shoot();
        }
        if (Input.GetKey(KeyCode.E) && currentPlanet != null)
        {
            currentPlanet.TakeOverPlanet(this);
            if(Input.anyKeyDown || alienCollide)
            {
                currentPlanet.StopTakeOver(this);   
            }
        }
        else if(Input.GetKeyUp(KeyCode.E) && currentPlanet != null)
        {
            currentPlanet.StopTakeOver(this);
        }

    }    
    private void FixedUpdate()
    {
        if (!isLocalPlayer || !GameState.isStart)
            return;
        float x = Input.GetAxisRaw("Horizontal");
        movement.Set(x , 0);
		if(prevMovement == Vector2.zero)
		{	if(movement.x < 0)
			{
			 	if(!previousDir)
				 {
                    rb.MoveRotation(rb.rotation *Quaternion.Euler(rotation));
					dir = 1;		
				 }
				previousDir = true;
			}
			else if(movement.x > 0)
			{
				if(previousDir)
				{		
                    rb.MoveRotation(rb.rotation *Quaternion.Euler(rotation));
                    dir = -1;
				}
				previousDir = false;
			}	
		}	
        movement = movement.normalized;
      // if(grounded)
		rb.MovePosition(rb.position + (dir * transform.TransformDirection(movement*movementSpeed)) * speed *Time.fixedDeltaTime);		
		prevMovement = movement;
    }
    void Shoot()
    {
        weapon.InitBullet();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "alienBullet" || collision.gameObject.tag == "meteor")
        {
            Damage();
        }
        if(collision.gameObject.tag == "alien")
        {
            if(!collision.gameObject.GetComponent<Alien>().IsSamePlayer(this))
            {
                alienCollide = true;
                Damage();
            }
        }
        else if(collision.gameObject.tag == "planet")
        {
            isGrounded = true;
        }
    }
    [Command]
    public void CmdGiveName(string s)
    {
        playerName = s;
    }
    public void Damage()
    {
        float damage = (type == playerType.player1) ? GameReference.Player1.weapon.GetWeaponDamage() :
        GameReference.Player2.weapon.GetWeaponDamage();
        CmdDamage(damage);
    }
    [ClientRpc]
    void RpcSetHpUI(int type , float damage)
    {
        hp_fillamount.fillAmount -= (damage*1.33f);//constant is the differece between hp range
        hp_slider[(int)type].fillAmount -= damage;
        IsDead(hp_slider[(int)type].fillAmount);
    }
    [Command]
    void CmdDamage(float damage )
    {
        RpcSetHpUI((int)type, damage);
        ParticleSystem particle = Instantiate(damage_particle) as ParticleSystem;
        particle.transform.position = transform.position;
        particle.Play();
        NetworkServer.Spawn(particle.gameObject);
    }
    public void IsDead(float fillamount)
    {
        if(fillamount <= 0.25)
        {
            MenuCode.instance.ShowWinner(true);
            CmdStopPlayer();
        }
           
    }
    public void AddHP(float amount)
    {
        hp_fillamount.fillAmount += amount;
    }
    [ClientRpc]
    public void RpcStopPlayer()
    {
        if(isServer)
            MenuCode.instance.ShowWinner(false);
        LanNetworkManager.instance.StopPlayers();
    }
    [Command]
    public void CmdStopPlayer()
    {
        MenuCode.instance.ShowWinner(false);
    }
    [Command]
    public void CmdTakeOverPlanet(GameObject obj)
    {
        if (isServer)
            RpcDeactive(obj);
        else
            CmdDeactive(obj);
        ParticleSystem particle = Instantiate(takeOver_particle) as ParticleSystem;
        particle.transform.position = transform.position;
        particle.transform.rotation = transform.rotation;
        NetworkServer.Spawn(particle.gameObject);
        GameObject fGO = Instantiate(flag) as GameObject;
        fGO.transform.position = transform.position;
        fGO.transform.rotation = transform.rotation;
        NetworkServer.Spawn(fGO);
        RpcTakeOverInit(fGO, (int)type);
    }
    [ClientRpc]
    public void RpcTakeOverInit(GameObject fGO , int type)
    {
        fGO.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().sprite = playerFlags[type];
        fGO.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite = playerNames[type];
    }
    public Planet GetCurrentPlanet()
    {
        return currentPlanet;
    }
    public void SetPlanetSprite(GameObject obj , int index)
    {
        if(isServer)
            RpcSetOrbitSprite(obj, index);
        else
            CmdSetOrbitSprite(obj , index);
    }
    [ClientRpc]
    void RpcDeactive(GameObject obj)
    {
        obj.SetActive(false);
    }
    [Command]
    void CmdDeactive(GameObject obj)
    {
        obj.SetActive(false);
        RpcDeactive(obj);
    }
    [ClientRpc]
    void RpcSetOrbitSprite(GameObject obj, int index)
    {
        obj.GetComponent<SpriteRenderer>().sprite = orbitSprites[index];
    }
    [Command]
    void CmdSetOrbitSprite(GameObject obj, int index)
    {
        obj.GetComponent<SpriteRenderer>().sprite = orbitSprites[index];
        RpcSetOrbitSprite(obj, index);
    }
 
}
