﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarmHole : Attractor
{
	[SerializeField]
	private WarmHole MoveTo;
	public float gravityRaduis;
	public Vector3 dir;
	bool firstDistance = true;
	public bool attractOut = false;
	void Port(Player obj)
	{
		MoveTo.dir = dir;
		obj.transform.position = MoveTo.transform.position;
		obj.rb.velocity = Vector3.zero;
		MoveTo.attractOut = true;
		firstDistance = true;
	}
	public void Attract(Player obj , Rigidbody objRb)
    {   
        if (IsInGravityZone(obj.transform.position , gravityRaduis))
        {
                Vector3 distance = (obj.transform.position - transform.position);
				if(firstDistance)
				{
					if(!attractOut)
						dir = -distance;
				}
				firstDistance = false;
				if(IsInGravityZone(obj.transform.position , 0.5f) && !attractOut)
				{
					Port(obj);
				}
                float force = mass * objRb.mass / (Mathf.Pow(attractOut ? 100*dir.magnitude : distance.magnitude, 2)) * gravityConstant;
                objRb.AddForce((attractOut ? 1 : -1)* force * (attractOut ? dir.normalized : distance.normalized));
                Quaternion targetRotation = Quaternion.FromToRotation(obj.transform.up,(attractOut ? dir.normalized : distance.normalized)) * obj.transform.rotation;
                obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, targetRotation, Time.deltaTime * 20f);
        }
		else
			attractOut = false;
    }
	private void Update() 
	{

			if (GameReference.Player2 != null)
			{
					Attract(GameReference.Player2, GameReference.Player2.rb);
			}
			if (GameReference.Player1 != null)
			{
					Attract(GameReference.Player1, GameReference.Player1.rb);
			}
		
	}
	bool IsInGravityZone(Vector3 position , float raduis)
    {
        if (Vector3.Distance(transform.position , position) <= raduis)
            return true;
        return false;
    }
	
}
