﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    [SerializeField]
    private RectTransform player1Logo;
    [SerializeField]
    private RectTransform player2Logo;
    [SerializeField]
    private Camera cam;
    Vector2 WorldObject_ScreenPosition1;
    Vector2 WorldObject_ScreenPosition2;

	void Update ()
    {
		if(GameReference.Player1 != null)
        {
            player1Logo.gameObject.SetActive(true);
            WorldObject_ScreenPosition1 = CalculatePosition(GameReference.Player1.transform , player1Logo);
            player1Logo.anchoredPosition = WorldObject_ScreenPosition1;
        }
        if (GameReference.Player2 != null)
        {
            player2Logo.gameObject.SetActive(true);
            WorldObject_ScreenPosition2 = CalculatePosition(GameReference.Player2.transform , player2Logo);
            player2Logo.anchoredPosition = WorldObject_ScreenPosition2;
        }
    }
    Vector2 CalculatePosition(Transform objPos , RectTransform canvasRect)
    {
        RectTransform CanvasRect = canvasRect;
        Vector2 ViewportPosition = cam.WorldToViewportPoint(objPos.position);
        return new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));
    }
}
