﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Borderforce : MonoBehaviour 
{
	public enum EdgeType{Up =0 , Down , Right , Left };
	public EdgeType type;
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "player")
		{			
			Player player = other.gameObject.GetComponent<Player>();
			if(type == EdgeType.Up)
				player.rb.AddForce(transform.up*100);
			else if(type == EdgeType.Down)
				player.rb.AddForce(-transform.up * 100);
			else  if (type == EdgeType.Right)
				player.rb.AddForce(transform.right * 100);
			else
				player.rb.AddForce(-transform.right * 100);
		} 

	}
}
