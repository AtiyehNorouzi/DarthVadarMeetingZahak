﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRotation : MonoBehaviour
{
    [SerializeField]
    private RectTransform [] positions;
    private RectTransform rectTransform;
    float speed = 0.8f;
    Vector3 dir;
    Vector3 target;
    int index = 0;
    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        target = positions[0].position;
    }
    void Update ()
    {
		if(Utility.AlmostEqual(rectTransform.position , target , 1f ))
        {
            if (index == positions.Length - 1)
                index = 0;
            else
                index++;
            target = positions[index].position;
            dir = (target - transform.position).normalized;
        }
        transform.Translate(dir * Time.deltaTime * speed);
	}
}
