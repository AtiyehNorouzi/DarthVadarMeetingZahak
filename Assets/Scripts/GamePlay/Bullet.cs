﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Bullet : NetworkBehaviour
{
    public enum bulletType { Normal = 0, Reload, Explode, Tame, Follow };
    [SerializeField]
    private ParticleSystem Bomb_particle;
    [SerializeField]
    private ParticleSystem[] player1_particles;
    [SerializeField]
    private ParticleSystem[] player2_particles;
    [SerializeField]
    private Sprite[] bulletSprites;
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private LineRenderer lr;
    bool follow = false;
    [SyncVar]
    public int type;
    int playerType;
    public void Init(Vector3 dir, float speed , bulletType t , int playerType)
    {
        this.playerType = playerType;
        RpcSetSprite((int)t);
        Move(dir, speed);
    }
    public void Init(Vector3 dir, float speed , bulletType t , GameObject player , int playerType)
    {
        this.playerType = playerType;
        RpcSetSprite((int)t);
        follow = true;
        StartCoroutine(FollowPlayer(player));
    }
    IEnumerator FollowPlayer(GameObject player)
    {
        float timer = 0;
        float followTime = 5f;
        while(follow)
        {
            timer += Time.deltaTime * 1f / followTime;
            if (Utility.AlmostEqual(timer, 1, 0.1f))
                Despawn();
            var dir = player.transform.position - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.position = Vector3.MoveTowards (transform.position, player.transform.position, Time.deltaTime * 2); 
            yield return null;
        }
    }
    [ClientRpc]
    void RpcSetSprite(int t)
    {
        type = t;
        spriteRenderer.sprite = bulletSprites[this.type]; 
    }

    [ClientRpc]
    public void RpcLazerShoot(Vector3 position , Vector3 dir)
    {
        spriteRenderer.sprite = null;
        lr.SetPosition(0, position);
        RaycastHit hit;
        if (Physics.Raycast(position, dir, out hit))
        {
            if (hit.collider)
            {
                lr.SetPosition(1, hit.point);
                if (hit.collider.gameObject.tag == "player")
                    hit.collider.gameObject.GetComponent<Player>().Damage();
            }
        }
        else
            lr.SetPosition(1, dir * 1000);
        lr.enabled = true;
        StartCoroutine(DelayDespawn(1));
    }
    void Move(Vector3 dir, float speed)
    {
        rb.AddForce(200 * dir);
    }
    private void OnCollisionEnter(Collision other)
    {
        if(type != 4)
        {
            if(other.gameObject.tag != "player")
                CmdCreateBomb();
        }
        else
        {
            if (other.gameObject.tag == "alien")
                other.gameObject.GetComponent<Alien>().OnCollideBullet(playerType == 0  ? GameReference.Player1 : GameReference.Player2);
        }
        Despawn();
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "border")
            Despawn();
    }
    [Command]
    void CmdCreateBomb()
    {
        ParticleSystem particle = Instantiate(playerType == 0 ? player1_particles[type] : player2_particles[type]) as ParticleSystem;
        particle.transform.position = transform.position;
        NetworkServer.Spawn(particle.gameObject);
    }
    IEnumerator DelayDespawn(float time)
    {
        yield return new WaitForSeconds(time);
        Despawn();
    }
    void Despawn()
    {
        follow = false;
        transform.position = new Vector3(-1 , 8 , 0);
        transform.rotation = Quaternion.identity;
        SendMessage("SetObjectInactive", SendMessageOptions.DontRequireReceiver);
    }
    void OnDisable()
    {
        if(!isServer)
            return;
        rb.velocity = Vector3.zero;
    }

}
