﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : Attractor 
{
	public enum holeType{Static , moving};
	public holeType type;
	public float gravityRaduis;

	void Update ()
	{
		if (GameReference.Player2 != null)
        {
                Attract(GameReference.Player2, GameReference.Player2.rb ,1);
                if(IsInGravityZone(GameReference.Player2.transform.position , 0.5f))
				{
					GameReference.Player2.CmdStopPlayer();
				}
        }
        if (GameReference.Player1 != null)
        {
                Attract(GameReference.Player1, GameReference.Player1.rb , 0);
                if(IsInGravityZone(GameReference.Player1.transform.position , 0.5f))
				{
					GameReference.Player1.CmdStopPlayer();
				}
        }	
	}
	bool IsInGravityZone(Vector3 position , float raduis)
    {
        if ((Mathf.Pow(transform.position.x - position.x , 2) + Mathf.Pow(transform.position.y - position.y , 2)) <= raduis)
            return true;
        return false;
    }
}
