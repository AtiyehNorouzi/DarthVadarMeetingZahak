﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    bool[] attract = { false, false, false };//PLAYER1 , PLAYER2 , ALIEN
    protected bool[] canTakeOver = { false, false, false };//PLAYER1 , PLAYER2 , ALIEN
    public float xAxis;
    public float yAxis;
	protected float gravityConstant = 6.674f;
	public float mass;
	public int attractPriotity = 0;

	public void Attract(Attractable obj , Rigidbody objRb , int attractIndex)
    {
        if (IsInGravityZone(obj.transform.position))
        {
            attract[attractIndex] = true;
            if (CheckPlanetsActive(attractIndex))
            {
                if (obj.isGrounded)
                {
                    if (obj.transform.parent != transform)
                    {
                        obj.transform.SetParent(transform);
                        obj.SetCurrentPlanet(this);
                    }
                    if (!canTakeOver[attractIndex])
                        canTakeOver[attractIndex] = true;
                }
                else
                {
                    if (canTakeOver[attractIndex])
                        canTakeOver[attractIndex] = false;
                }
                Vector3 distance = GetDistance(obj.transform.position, transform.position);
                float force = mass * objRb.mass / Mathf.Pow(distance.magnitude, 2) * gravityConstant;
                objRb.AddForce(-1 * force * distance.normalized);
                Quaternion targetRotation = Quaternion.FromToRotation(obj.transform.up, distance.normalized) * obj.transform.rotation;
                obj.transform.rotation = Quaternion.Slerp(obj.transform.rotation, targetRotation, Time.deltaTime * 20f);
            }
            else
            {
                if (canTakeOver[attractIndex])
                    canTakeOver[attractIndex] = false;
            }
        }
        else
        {
            if (attract[attractIndex] || canTakeOver[attractIndex])
            {
                attract[attractIndex] = false;
                canTakeOver[attractIndex] = false;
            }
        }
    }
	Vector3 GetDistance(Vector3 p1 , Vector3 p2){
        return new Vector3(p1.x - p2.x , p1.y - p2.y , 0);
    }
	 bool CheckPlanetsActive (int attractIndex) {
        for (int i = 0; i < GameReference.Instance.planets.Length; i++) {
            if (GameReference.Instance.planets[i].attract[attractIndex] && GameReference.Instance.planets[i].attractPriotity > attractPriotity)
                return false;
        }
        return true;
    }
    bool IsInGravityZone(Vector3 position)
    {
        Vector3 distance = (position - transform.position);
        if (((Mathf.Pow(distance.x, 2) / (xAxis * xAxis)) + (Mathf.Pow(distance.y, 2) / (yAxis * yAxis))) <= 1.2f)
            return true;
        return false;
    }
}
