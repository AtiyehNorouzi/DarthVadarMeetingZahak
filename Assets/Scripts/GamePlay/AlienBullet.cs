﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienBullet : MonoBehaviour
{

    public void Init(Vector3 dir, float speed)
    {

        StartCoroutine(Move(dir, speed));
    }
    IEnumerator Move(Vector3 dir , float speed)
    {
        while (true)
        {
            transform.Translate(dir * speed *Time.deltaTime , Space.World);
            yield return null;
        }
    }
}
