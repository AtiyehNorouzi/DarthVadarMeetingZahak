﻿using System;
using UnityEngine.Networking;
using Debug = UnityEngine.Debug;
using System.IO;
using Microsoft.Win32;
using UnityEngine;

public class LANDiscoveryManager : NetworkDiscovery
{        //This is the location of the firewall exceptions list in Windows 10
    const string firewallRegistry = "SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile\\AuthorizedApplications\\List";

    private void Start()
    {
        //This goes in your setup method.
        RegistryKey key = Registry.LocalMachine.CreateSubKey(firewallRegistry);
        key.SetValue(AppPath(), AppPath() + ":*:Enabled:" + Application.productName + " application");
    }
    //Doing this also requires
    //using System.IO;
    //It just gets the path to the exe file.
    public string AppPath()
    {
        return Path.GetFullPath(".") + "\\" + Application.productName + ".exe";
    }
    public Action<string,string> OnServerFound;

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        if (OnServerFound != null)
        {
            OnServerFound(fromAddress, data);
        }
    }

}
