﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HostInstance : MonoBehaviour
{
    [SerializeField]
    private Sprite[] characters; // zahak , darthvadar
    [SerializeField]
    private Image character_image;
    string ipAddress;
    public void JoinGame()
    {
        MenuCode.instance.SetJoinIpAddress(ipAddress);
    }
    public void Init(string ipAddress , int characterNumber)
    {
        this.ipAddress = ipAddress;
        character_image.sprite = characters[characterNumber];
    }
}
