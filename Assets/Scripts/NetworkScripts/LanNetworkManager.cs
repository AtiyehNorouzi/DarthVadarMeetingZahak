﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class LanNetworkManager : NetworkManager
{
    public string playerName = "Guest";
    public LANDiscoveryManager discovery;
    private static LanNetworkManager _instance;
    public static LanNetworkManager instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<LanNetworkManager>();
            return _instance;
        }
    }
    public int servertype = 0;
    public void CreateDedicatedServer(string data , int type , string playerName)
    {
        this.playerName = playerName;
        servertype = type;
        networkPort = 7777;
        NetworkManager.singleton.networkPort = 7777;
        instance.discovery.broadcastData = data;
        instance.discovery.Initialize();
        instance.discovery.StartAsServer();
        NetworkManager.singleton.StartHost();
    }
    public int GetServerType()
    {
        return servertype;
    }
    public void ConnectLocally(string serverAddress, int serverPort, bool discovery , int type , string playerName)
    {
        this.playerName = playerName;
        servertype = type;
        NetworkManager.singleton.networkAddress = serverAddress;
        NetworkManager.singleton.networkPort = serverPort;
        NetworkManager.singleton.StartClient();
    }
    public void StopPlayers()
    {
        instance.discovery.StopBroadcast();
        NetworkManager.singleton.StopHost();
        StopClient();
    }
}
