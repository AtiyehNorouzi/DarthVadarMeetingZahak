﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
public class NHNetworkedPool : NetworkBehaviour {

    public string poolname="";
    public GameObject template;
    public int poolsize;

    public List<GameObject> ObjectList;
    protected Queue<GameObject> AvailableObjects;

    public bool AutoResize = true;
    Vector3 startPosition = new Vector3(-1, 8, 0);
    GameObject ClientSpawnHandler(Vector3 position, NetworkHash128 assetId)
    {
        var go = InstantiatePrefab();
        return go;
    }

    void ClientUnSpawnHandler(GameObject spawned)
    {
        spawned.GetComponent<NHNetworkedPoolObject>().SetObjectInactive();
    }

    void Awake()
    {
        ClientScene.RegisterSpawnHandler(template.GetComponent<NetworkIdentity>().assetId, ClientSpawnHandler, ClientUnSpawnHandler);
    }

	void Start () {

        if( NetworkServer.active )
        {
            ObjectList = new List<GameObject>();
            AvailableObjects = new Queue<GameObject>();

            for (int i = 0; i < poolsize; i++)
            {
                ObjectList.Add(InstantiatePrefab());
                AvailableObjects.Enqueue(ObjectList[ObjectList.Count-1]);
            }
        }

	}
    GameObject InstantiatePrefab()
    {
        GameObject obj = Instantiate(template, startPosition , Quaternion.identity) as GameObject;
        obj.transform.parent = transform;
        NHNetworkedPoolObject nhobj = obj.AddComponent<NHNetworkedPoolObject>();
        nhobj.SetObjectInactive();
        if(isServer)
            NetworkServer.Spawn(obj);

        return obj;
    }

	public bool InstantiateFromPool(Vector3 pos, Quaternion rot, out GameObject obj) {
        if (!isServer) { obj = null; return false; }

        if (AvailableObjects.Count > 0)
        {
            GameObject GO = AvailableObjects.Dequeue();
            GO.transform.position = pos;
            GO.transform.rotation = rot;
            GO.GetComponent<NHNetworkedPoolObject>().SetObjectActive();
            obj = GO;
            return true;
        }

        if (AutoResize)
        {
            GameObject g = InstantiatePrefab();
            g.transform.position = pos;
            g.transform.rotation = rot;
            g.GetComponent<NHNetworkedPoolObject>().SetObjectActive();
            ObjectList.Add(g);
            obj = g;
            return true;
        }
        else
        {
            obj = null;
            return false;
        }
	}
    public void AddToAvailableObjects(GameObject obj)
    {
        if (!isServer) return;

        AvailableObjects.Enqueue(obj);
    }
}
