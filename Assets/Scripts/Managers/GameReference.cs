﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameReference : MonoBehaviour {
   
    private static GameReference instance;
    public static GameReference Instance {
        get {
            if (instance == null)
                instance = GameObject.FindObjectOfType<GameReference> ();
            return instance;
        }
    }
    private void Awake () {
        instance = this;
    }
    private static Player player1;
    public static Player Player1 {
        get {
            return player1;
        }
        set {
            player1 = value;
        }
    }
    private static Player player2;
    public static Player Player2 {
        get {
            return player2;
        }
        set {
            player2 = value;
        }
    }
    public Color[] lineColors;
    [System.Serializable]
    public struct WeaponStruct 
    {
        public float bulletSpeed;
        public float bulletCapacity;
        public float currentCapacity;
        public float timeToShoot;
        public float inffectRaduis;
        public float timer;
        public bool canShoot;
        public bool reload;
        public bool OnReload;
        public float damageRate;
        public GameObject weaponUI;
        public Text currentCapacity_txt;
        public Image timeToShoot_slider;
        public Image reload_slider;
    }

    public Sprite[] player1WeaponSprites;
    public Sprite[] player2WeaponSprites;
    public WeaponStruct[] player1Weapons;
    public WeaponStruct[] player2Weapons;
    public GameObject[] PoolObjPrefabs;
    
    public Image [] player_hps;
    public GameObject player1UIPanel;
    public GameObject player2UIPanel;
    public int[] PoolObjAmounts;
    public  Planet[] planets;
    public  Sprite[] orbitSprites;
}