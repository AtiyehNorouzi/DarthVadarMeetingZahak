﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    [SerializeField]
    private GameObject mainCamera;
    private GameObject playerToFollow;
    private Vector3 cameraOffset = new Vector3(3, 0, -5);
	void Update()
	{
            if (playerToFollow != null )
                mainCamera.transform.position = playerToFollow.transform.position + cameraOffset;
    }
    public void AssignPlayer(GameObject player)
    {
        if (playerToFollow == null)
        {
            playerToFollow = player;
        }
    }
}
