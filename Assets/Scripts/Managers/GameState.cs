﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GameState : NetworkBehaviour
{

    public static bool isStart = false;
    bool set = false;
    private void Update()
    {
        if(isStart && !set && isServer)
        {
            RpcSetIsStart();
            set = true;
        }
    }
    [ClientRpc]
    void RpcSetIsStart()
    {
        GameState.isStart = true;
    }
}
