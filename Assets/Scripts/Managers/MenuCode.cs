﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class MenuCode : MonoBehaviour
{
    //data structures
    Dictionary<string, NetworkBroadcastResult> recievedIPs = new Dictionary<string, NetworkBroadcastResult>();

    //properties
    private static MenuCode _instance;
    public static MenuCode instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<MenuCode>();
            return _instance;
        }
    }
    //UI
    [SerializeField]
    private GameObject menuPanel;

    [SerializeField]
    private GameObject createGamePanel;

    [SerializeField]
    private GameObject joinGamePanel;

    [SerializeField]
    private GameObject gameInstancePrefab;

    [SerializeField]
    private GameObject matchListParent;

    [SerializeField]
    private InputField[] playerName_txt;//create/join

    [SerializeField]
    private InputField serverIp_txt;

    [SerializeField]
    private Button createGame_btn;

    [SerializeField]
    private GameObject WinnerPanel;

    [SerializeField]
    private GameObject back_btn;

    //variables
    int type = -1;
    const int port = 7777;

    public void StartAsHost()
    {
        if (type >= 0)
        {
            if (playerName_txt[0].text != "")
                LanNetworkManager.instance.CreateDedicatedServer(playerName_txt[0].text + "," + type.ToString(), type, playerName_txt[0].text);
            else
                LanNetworkManager.instance.CreateDedicatedServer("Guest" + "," + type.ToString(), type, "Guest");
        }
    }
    public void StartAsClient()
    {
        if (playerName_txt[1].text != "")
            LanNetworkManager.instance.ConnectLocally(serverIp_txt.text, port, false, type, playerName_txt[1].text);
        else
            LanNetworkManager.instance.ConnectLocally(serverIp_txt.text, port, false, type, "Guest");
        DisableMenus();
    }
    void FindAvailableMatches()
    {
        LanNetworkManager.instance.discovery.Initialize();
        LanNetworkManager.instance.discovery.StartAsClient();
        LanNetworkManager.instance.discovery.OnServerFound += (address, data) =>
        {
            if (recievedIPs.ContainsKey(address))
                return;
            type = int.Parse(data.Split(',')[1]);
            GameObject gameInstance = Instantiate(gameInstancePrefab, matchListParent.transform);
            gameInstance.GetComponentInChildren<Text>().text = address + "   : " + data.Split(',')[0];
            gameInstance.GetComponent<HostInstance>().Init(address, type);
            recievedIPs.Add(address, LanNetworkManager.instance.discovery.broadcastsReceived[address]);

        };

    }
    public void ShowWinner(bool lose)
    {
        WinnerPanel.transform.parent.gameObject.SetActive(true);
        WinnerPanel.SetActive(true);
        WinnerPanel.transform.GetChild(0).gameObject.SetActive(lose);
        WinnerPanel.transform.GetChild(1).gameObject.SetActive(!lose);
    }
    public void SetJoinIpAddress(string ip)
    {
        serverIp_txt.text = ip;
    }
    public void DisableMenus()
    {
        menuPanel.SetActive(false);
        createGamePanel.SetActive(false);
        joinGamePanel.SetActive(false);
        menuPanel.transform.parent.gameObject.SetActive(false);

    }
    public void Restart()
    {
        type = -1;
        LanNetworkManager.instance.StopPlayers();
        SceneManager.LoadScene(0);
    }
    public void CreateGameClicked()
    {
        back_btn.SetActive(true);
        menuPanel.SetActive(false);
        createGamePanel.SetActive(true);
    }
    public void JoinGameClicked()
    {
        back_btn.SetActive(true);
        menuPanel.SetActive(false);
        joinGamePanel.SetActive(true);
    }
    public void BackToMenu()
    {
        type = -1;
        back_btn.SetActive(false);
        joinGamePanel.SetActive(false);
        menuPanel.SetActive(true);
        createGamePanel.SetActive(false);
        WinnerPanel.SetActive(false);
    }
    public void OnCharacterClicked(int type)
    {
        this.type = type;
        createGame_btn.interactable = true;
    }
    public void FindMatch()
    {
        FindAvailableMatches();
    }
}
