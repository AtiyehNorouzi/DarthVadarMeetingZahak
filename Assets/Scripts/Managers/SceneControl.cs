﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class SceneControl : NetworkBehaviour
{
    //prefab and transforms
    [SerializeField]
    private GameObject[] ammos;
    [SerializeField]
    private GameObject[] meterorPrefabs;
    [SerializeField]
    private Transform [] meteorTransforms;
    //property
    private static SceneControl instance;
    public static SceneControl Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<SceneControl>();
            return instance;
        }
    }
    //Ammo Infos
    int maxAmmos = 6;
    int activeAmmos = 0;
    int minTimeToGenerate = 10;
    int maxTimeToGenerate = 15;
    //Meteor Infos
    const float MeteorGenerateTime =  10f;
    float minMTimeToGenerate = 0.5f;
    float maxMTimeToGenerate = 1f;
    //global Use
    float timer = 0;
    bool generate = false;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if(GameState.isStart && isServer && hasAuthority && !generate)
        {
            StartGenerate();
            generate = true;
        }
    }
    public void StartGenerate()
    {
        StartCoroutine(Generate());
        StartCoroutine(GenerateMeteor());
    }
    IEnumerator TimeCounter(float time)
    {
        timer = 0;
        while(true)
        {
            timer += Time.deltaTime * 1f / time;
            yield return null;
        }
    }
    IEnumerator GenerateMeteor()
    {
        int numOfTry = 6;
        List<int> positions = new List<int>();
        Coroutine timeRoutine = StartCoroutine(TimeCounter(MeteorGenerateTime));
        yield return new WaitForSeconds(MeteorGenerateTime * 2);
        while(true)
        {
            if (!Utility.AlmostEqual(timer, 1, 0.1f))
            {
                int meteorCount = Random.Range(1, meteorTransforms.Length);
                for (int i = 0; i < meteorCount; i++)
                {
                    int random = Random.Range(0, meteorTransforms.Length);
                    while (positions.Contains(random) && numOfTry >= 0)
                    {
                        random = Random.Range(0, meteorTransforms.Length);
                        positions.Add(random);
                        numOfTry--;
                    }
                    if (numOfTry >= 0)
                    {
                        int randPrefab = Random.Range(0, meterorPrefabs.Length);
                        CmdCreateMeteor(randPrefab, random);
                    }
                }
                yield return new WaitForSeconds(Random.Range(minMTimeToGenerate, maxMTimeToGenerate));
            }
            else
            {
                StopCoroutine(timeRoutine);
                positions.Clear();
                yield return new WaitForSeconds(Random.Range(MeteorGenerateTime * 2, MeteorGenerateTime * 4));
                timeRoutine = StartCoroutine(TimeCounter(MeteorGenerateTime));
            }
            yield return null;
        }
    }
    IEnumerator Generate()
    {
        while (true)
        {
            if(activeAmmos < maxAmmos)
            {
                int count = 0;
                int random = Random.Range(0, ammos.Length);
                while(ammos[random].activeInHierarchy && count < 5)
                {
                    random = Random.Range(0, ammos.Length);
                    count++;
                }
                if(count <= 5)
                {
                    RpcSetAmmoActive(random);
                    activeAmmos++;
                    yield return new WaitForSeconds(Random.Range(minTimeToGenerate, maxTimeToGenerate));
                }     
            }
            yield return null;
        }

    }
    [Command]
    void CmdCreateMeteor(int randPrefab , int randomMeteor)
    {
        GameObject Go = Instantiate(meterorPrefabs[randPrefab]);
        Go.transform.SetParent(meteorTransforms[randomMeteor]);
        Go.transform.localPosition = Vector3.zero;
        NetworkServer.Spawn(Go);
    }
    [ClientRpc]
    void RpcSetAmmoActive(int random)
    {
        ammos[random].SetActive(true);
        ammos[random].GetComponent<Ammo>().EnableCollider(true);
    }
   
    public void RemoveAmmo()
    {
        activeAmmos--;
    }
}
